#include <iostream>
#include <random>
#include <string>
#include <SFML/Graphics.hpp>


#define SHOW(x) std::cout << #x << ": " << x << "\n";

sf::Font font = [](){
    sf::Font f;
    if (!f.loadFromFile("./sansation.ttf"))
        throw "sansation.ttf not found";
        return f;
    }();


enum class cell {dead, alive};

struct board_t {
    int nx, ny;
    std::vector<cell> data;
    board_t(int nx, int ny) : nx{nx}, ny{ny}, data(nx*ny)
    {}

    cell operator()(int i, int j) const {
        return data[i*ny+j];
    }

    cell& operator()(int i, int j) {
        return data[i*ny+j];
    }
};

int wrap(int i, int ni) {
    if ( i < 0 ) return ni-1;
    if ( i >= ni) return 0;
    return i;
}

int main(int, char **)
{

    int win_x = 1200, win_y = 900;

    sf::RenderWindow window(sf::VideoMode(win_x, win_y), "Conway Game of Life");

    int nx = 40, ny = 40;

    board_t board1{nx, ny};
    board_t board2{nx, ny};

    board_t *board_read = &board1, *board_write = &board2;

    bool setting_up = true;
    bool clicked = false;
    bool slowdown = true;

    sf::Vector2i previous_cell;

    int generation = 0;

    sf::Clock clock;
    float time = 500.f;
    bool random = false;

    std::random_device gen;
    std::uniform_int_distribution<int> int_dist(1,100);

    bool clear = false;

    while (window.isOpen())
    {
        // Process events
        sf::Event event;
        while (window.pollEvent(event))
        {
            // Close window: exit
            if (event.type == sf::Event::Closed)
                window.close();

            if (event.type == sf::Event::KeyPressed)
            {
                switch (event.key.code)
                {
                case sf::Keyboard::Escape:
                    window.close();
                    break;
                case sf::Keyboard::Space:
                {
                    setting_up = !setting_up;
                    break;
                }
                case sf::Keyboard::S:
                {
                    slowdown = !slowdown;
                    break;
                }
                case sf::Keyboard::V:
                {
                    time += 5;
                    break;
                }
                case sf::Keyboard::L:
                {
                    time -= 5;
                    break;
                }
                case sf::Keyboard::R:
                {
                    random = true;
                    break;
                }
                case sf::Keyboard::C:
                {
                    clear = true;
                    break;
                }
                default:
                    break;
                }
            }
        }
        //window.clear();

        if (setting_up) {
            generation=0;
            if (random) {
                random = false;
                for (int i=0; i<nx; ++i) {
                    for (int j=0; j<ny; ++j) {
                        if (int_dist(gen) < 30) {
                            (*board_write)(i,j) = cell::alive;
                        }
                    }
                }
            }
            if (clear) {
                clear = false;
                for (int i=0; i<nx; ++i) {
                    for (int j=0; j<ny; ++j) {
                        (*board_write)(i,j) = cell::dead;
                    }
                }
            }
            if (clicked == false && sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
                clicked = true;
                auto pos = sf::Mouse::getPosition(window);
                int i = pos.x * nx / win_x;
                int j = pos.y * ny / win_y;
                previous_cell = sf::Vector2i{i,j};
                if ((*board_write)(i,j) == cell::alive) {
                    (*board_write)(i,j) = cell::dead;
                } else {
                    (*board_write)(i,j) = cell::alive;
                }
            } else
                if (clicked == true && sf::Mouse::isButtonPressed(sf::Mouse::Left))
                {
                    auto pos = sf::Mouse::getPosition(window);
                    int i = pos.x * nx / win_x;
                    int j = pos.y * ny / win_y;
                    if (previous_cell != sf::Vector2i{i,j}) {
                        if ((*board_write)(i,j) == cell::alive) {
                            (*board_write)(i,j) = cell::dead;
                        } else {
                            (*board_write)(i,j) = cell::alive;
                        }
                    }
                    previous_cell = sf::Vector2i{i,j};
                } else {
                clicked = false;
            }
        } else {

            board_t* tmp = board_write;
            board_write = board_read;
            board_read = tmp;

            generation++;

            for (int i=0; i<nx; ++i) {
                for (int j=0; j<ny; ++j) {
                    int count = 0;
                    for (int ii = -1; ii <= 1; ++ii) {
                        for (int jj = -1; jj <= 1; ++jj) {
                            if (! (ii == 0 && jj == 0)) {
                                count += ((*board_read)(wrap(i+ii, nx), wrap(j+jj, ny)) == cell::alive)?1:0;
                            }
                        }
                    }
                    if ((*board_read)(i,j) == cell::alive) {
                        if (count == 2 || count == 3) {
                            (*board_write)(i,j) = cell::alive;
                        } else {
                            (*board_write)(i,j) = cell::dead;
                        }
                    } else {
                        if (count == 3) {
                            (*board_write)(i,j) = cell::alive;
                        } else {
                            (*board_write)(i,j) = cell::dead;
                        }
                    }
                }
            }
        }

        sf::RectangleShape rect(sf::Vector2f{1.0f*win_x/nx, 1.0f*win_y/ny});
        rect.setOutlineColor(sf::Color::Red);
        for (int i=0; i<nx; ++i) {
            for (int j=0; j<ny; ++j){
                if ((*board_write)(i,j) == cell::alive) {
                    rect.setFillColor(sf::Color::Black);
                } else {
                    rect.setFillColor(sf::Color::White);
                }
                rect.setPosition(sf::Vector2f{i*1.0f*win_x/nx, j*1.0f*win_y/ny});
                rect.setOutlineThickness(3.0f);
                window.draw(rect);
            }
        }
        sf::Text text(std::string("Generatrion: ") + std::to_string(generation) + " dT " + std::to_string((int)time), font, 50);
        text.setFillColor(sf::Color::Red);
        window.draw(text);

        // Clear screen
        window.display();
        if (!setting_up and slowdown) {
            while (clock.getElapsedTime().asMilliseconds() < time) {
            }
            clock.restart();
        }

    }
}

